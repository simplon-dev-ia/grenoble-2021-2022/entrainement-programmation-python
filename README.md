# Entrainement Programmation Python

> Activités d'entrainement à la programmation Python

Les activités contenues dans ce dépôt ont pour but de s'entraîner à la
programmation avec le langage Python.

Les activités sont sous forme de notebooks interactifs et sont à réaliser
idéalement en _pair-programming_.

## Mise en place

Utiliser le programme `pipenv` pour créer un environnement virtuel Python
avec les dépendances adéquates :

```bash
pipenv install
pipenv shell
```

Il suffit ensuite de démarrer Jupyter Notebook pour manipuler les notebooks :

```bash
jupyter notebook
```

## Entrainements

Les entrainements sont classés par difficulté progressive :

1. [Notions](01_notions.ipynb)
2. [Fichiers](02_fichiers.ipynb)
3. [Modules](03_modules.ipynb)
